//Created by Eric Goldner
if(typeof($)!="function"){$=function(id){return document.getElementById(id);}}
Array.prototype.Copy=function(){if(this.length==0){return[];}
else if(typeof(this[0])=="object"){var $a=new Array(this.length);for(var $b=0;$b<this.length;$b++){if(this[$b]===null){$a[$b]=null;}
else if(this[$b].constructor.toString().indexOf("Array")!=-1){$a[$b]=[];for(var y=0;y<this[$b].length;y++){if(typeof(this[$b][y].Clone)=="function"){$a[$b].push(this[$b][y].Clone());}
else if(this[$b][y]===null){$a[$b].push(null);}}}else if(typeof(this[$b].Clone)=="function"){$a[$b]=this[$b].Clone();}}
return $a;}}
Array.prototype.shuffle=function($c){var i=this.length,j,t;while(i){j=Math.floor((i--)*Math.random());t=$c&&typeof this[i].shuffle!=='undefined'?this[i].shuffle():this[i];this[i]=this[j];this[j]=t;}
return this;}
function Board($d){this.boardId=$d
this.board=new Array(8);for(i=0;i<this.board.length;i++){this.board[i]=[];}
this.initialBoard=null;this.previousBoard=null;this.freeCells=null;this.foundations=null;this.selectedCard=null;this.lastClickTime=null;}
Board.prototype.Deal=function(){this.freeCells=[null,null,null,null];this.foundations=[null,null,null,null];this.selectedCard=[null,null];this.lastClickTime=null;var $e=[];var $f=[1,2,3,4,5,6,7,8,9,10,11,12,13];var $g=['s','c','h','d'];var $h=null;for(i=0;i<$g.length;i++){for(j=0;j<$f.length;j++){$h=new Card($f[j],$g[i]);$e.push($h);}}
$e.shuffle();$e.shuffle();$e.shuffle();$e.shuffle();var j=-1;while($e.length>0){j++;for(i=0;i<this.board.length;i++){if($e.length>0){this.board[i][j]=$e.pop();}}}
this.initialBoard=[this.freeCells.Copy(),this.foundations.Copy(),this.board.Copy()];this.SaveState();this.Draw();}
Board.prototype.ShowInitial=function(){var $i=[];for(var x=0;x<this.previousBoard[2].length;x++){$i.push('<div style="float:left;width:50px;">');for(var y=0;y<this.previousBoard[2][x].length;y++){$i.push(this.previousBoard[2][x][y].number,this.previousBoard[2][x][y].suit,"<br>");}
$i.push('</div>');}
document.getElementById('debug').innerHTML=$i.join("");}
Board.prototype.Draw=function(){var $j=null;var $h=null;var $k=this;var i=0;var $l=35;var $m=false;do{$m=false;for(i=0;i<this.freeCells.length;i++){if(this.freeCells[i]!==null&&this.CanAutoMoveToFoundation(this.freeCells[i])){$m=true
this.MM("fc",i,"found",null,true,false);}}
for(i=0;i<this.board.length;i++){if(this.CanAutoMoveToFoundation(i)){$m=true
this.MM("b",i,"found",null,true,false);}}}while(i<20&&$m==true)
ClearChildren(document.getElementById(this.boardId));for(x=0;x<this.freeCells.length;x++){$j=document.createElement("div");$j.id="freecell_"+x;$j.style.top=$l+"px";$j.style.left=(x*(Card.width+2))+"px";if(this.freeCells[x]===null){$j.className="placeholder";}
else if(this.selectedCard[0]=='fc'&&this.selectedCard[1]==x){$j.className="card";var $n=this.freeCells[x].GetImageCoords(true);$j.style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";}else{$j.className="card";var $n=this.freeCells[x].GetImageCoords(false);$j.style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";}
$j.col=x;$j.onmouseover=function(e){document.getElementById('kingshead').className="kingLookingLeft";}
$j.onclick=function(e){if($k.selectedCard[0]===null){$k.selectedCard=['fc',this.col];$k.Draw();return;}
else{if(!($k.selectedCard[0]=='fc'&&$k.selectedCard[1]==this.col)){$k.MM($k.selectedCard[0],$k.selectedCard[1],"fc",this.col,true,true);}
$k.selectedCard=[null,null];$k.Draw();}}
document.getElementById(this.boardId).appendChild($j);}
var $o=(this.freeCells.length*(Card.width+2));$j=document.createElement("div");$j.id="kingshead";$j.style.position="absolute";$j.style.top=$l+"px";$j.style.left=$o+"px";$j.className="kingLookingLeft";document.getElementById(this.boardId).appendChild($j);$o=((1+this.freeCells.length)*(Card.width+2));for(x=0;x<this.foundations.length;x++){$j=document.createElement("div");$j.id="foundation_"+x;$j.style.top=$l+"px";$j.style.left=($o+(x*(Card.width+2)))+"px";if(this.foundations[x]===null){$j.className="placeholder";}else{$j.className="card";var $n=this.foundations[x].GetImageCoords(false);$j.style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";}
$j.col=x;$j.onmouseover=function(e){document.getElementById('kingshead').className="kingLookingRight";}
$j.onclick=function(e){if($k.selectedCard[0]===null){}
else{$k.MM($k.selectedCard[0],$k.selectedCard[1],"found",this.col,true,true);$k.selectedCard=[null,null];$k.Draw();}}
document.getElementById(this.boardId).appendChild($j);}
if(this.GetNumOpenFreeCells()==this.freeCells.length&&this.GetNumOpenCols()==this.board.length){document.getElementById('winFace').style.display="block";alert("Congratulations, you win!");document.getElementById('winFace').style.display="none";this.Deal();return;}else{for(x=0;x<this.board.length;x++){if(this.board[x].length>0){for(y=0;y<this.board[x].length;y++){$h=this.board[x][y];if($h!=null){$j=document.createElement("div");$j.id="tableau_"+x+"-"+y;if(this.selectedCard[0]=='b'&&this.selectedCard[1]==x&&y==this.board[x].length-1){$j.className="card";var $n=$h.GetImageCoords(true);$j.style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";}else{$j.className="card";var $n=$h.GetImageCoords(false);$j.style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";}
$j.style.top=$l+100+(y*20+5)+"px";$j.style.left=(x*(7+Card.width)+5)+"px";$j.style.zIndex=y+10;$j.col=x;$j.row=y;if(BD.browser=="Explorer"){$j.ondblclick=function(){if($k.MM("b",this.col,"fc",0,true,true)){$k.selectedCard=[null,null];$k.Draw();}}}
$j.oncontextmenu=function(){if(this.style.zIndex==5000){this.style.zIndex=this.row+10;}else{this.style.zIndex=5000;}
return false;}
$j.onmouseout=function(e){if(this.style.zIndex==5000){this.style.zIndex=this.row+10;}}
$j.onclick=function(){var $p=$k.MCI();if($k.selectedCard[0]===null){$k.selectedCard=['b',this.col];var $q=$k.board[this.col].length-1;var $n=$k.board[this.col][$q].GetImageCoords(true);document.getElementById('tableau_'+this.col+'-'+$q).style.backgroundPosition="-"+$n[0]+"px -"+$n[1]+"px";return true;}
else{if($k.selectedCard[0]=='b'&&$k.selectedCard[1]==this.col){if(BD.browser!="Explorer"&&$p<=250){if($k.MM($k.selectedCard[0],$k.selectedCard[1],"fc",0,true,true)){}}}
else{$k.MM($k.selectedCard[0],$k.selectedCard[1],"b",this.col,true,true);}
$k.selectedCard=[null,null];$k.Draw();}}
document.getElementById(this.boardId).appendChild($j);}}}else{$j=document.createElement("div");$j.className="placeholder";$j.style.top=$l+100+(5)+"px";$j.style.left=(x*(7+Card.width)+5)+"px";$j.col=x;$j.row=-1;$j.onclick=function(e){var $p=$k.MCI();if($k.selectedCard[0]!==null){if(!($k.selectedCard[0]=='b'&&$k.selectedCard[1]==this.col)){$k.MM($k.selectedCard[0],$k.selectedCard[1],"b",this.col,true,true);}
$k.selectedCard=[null,null];$k.Draw();}}
document.getElementById(this.boardId).appendChild($j);}}}}
Board.prototype.MCI=function(){if(this.lastClickTime===null){this.lastClickTime=new Date().getTime();return null;}else{var $r=new Date().getTime();var $a=$r-this.lastClickTime;this.lastClickTime=$r;return $a;}}
Board.prototype.MM=function($s,$t,$u,$v,$w,$x){if($s==$u&&$t==$v){return false;}
var $y=[];var $z=null;var $A=null;if($s=='b'){for(i=this.board[$t].length-1;i>=0;i--){$z=this.board[$t][i];if($y.length>0){$A=$y[$y.length-1];}
if($A===null||(parseInt($z.number)==parseInt($A.number)+1&&$z.GetColor()!=$A.GetColor())){$y.push($z);}else{break;}}}
else if($s=='fc'){$y.push(this.freeCells[$t]);}
if($y.length==0||$y[0]==null){return false;}
if($u=='fc'){var $m=false;if($s=='b'){while(this.CanAutoMoveToFoundation($t)){return this.MM($s,$t,"found",0,$w);}}
for(i=0;i<this.freeCells.length;i++){if($y.length>0&&this.freeCells[i]===null){if($w){if($x){this.SaveState();}
this.freeCells[i]=$y[0];if($s=='fc'){this.freeCells[$t]=null;}
else if($s=='b'){this.board[$t].pop();}}
$m=true;break;}}
return $m;}
else if($u=='b'){var $z=null;if(this.board[$v].length>0){$z=this.board[$v][this.board[$v].length-1];}
var $B=0;if($z===null){$B=Math.min($y.length,(this.GetNumOpenFreeCells()+1)*this.GetNumOpenCols());}else{$B=Math.min($y.length,(this.GetNumOpenFreeCells()+1)*(this.GetNumOpenCols()+1));}
for(i=$B-1;i>=0;i--){$A=$y[i];if($z===null||(parseInt($A.number)+1==parseInt($z.number)&&$A.GetColor()!=$z.GetColor())){if($w){if($x){this.SaveState();}
$y.reverse();this.board[$v]=this.board[$v].concat($y.splice($y.length-(i+1),i+1));if($s=='b'){this.board[$t].splice(this.board[$t].length-(i+1),i+1);}
else if($s=='fc'){this.freeCells[$t]=null;}}
return true;}}
return false;}
else if($u=='found'){$A=$y[0];for(var $C=0;$C<4;$C++){if((this.foundations[$C]==null&&$A.number=="1")||(this.foundations[$C]!=null&&parseInt(this.foundations[$C].number)+1==parseInt($A.number)&&this.foundations[$C].suit==$A.suit)){if($w){if($x){this.SaveState();}
this.foundations[$C]=$A;if($s=='fc'){this.freeCells[$t]=null;}
else if($s=='b'){this.board[$t].pop();}}
return true}}
return false;}}
Board.prototype.SaveState=function(){this.previousBoard=[this.freeCells.Copy(),this.foundations.Copy(),this.board.Copy()];}
Board.prototype.Undo=function(){if(this.previousBoard==null){return false;}
this.freeCells=this.previousBoard[0].Copy();this.foundations=this.previousBoard[1].Copy();this.board=this.previousBoard[2].Copy();this.previousBoard=null;this.Draw();}
Board.prototype.Restart=function(){if(this.initialBoard==null){return false;}
this.freeCells=this.initialBoard[0].Copy();this.foundations=this.initialBoard[1].Copy();this.board=this.initialBoard[2].Copy();this.Draw();}
Board.prototype.CanAutoMoveToFoundation=function($D){var $A=null;if(typeof($D)=="number"){if(this.board[$D].length==0){return false;}
$A=this.board[$D][this.board[$D].length-1];}else{$A=$D;}
var $E=$A.GetColor();var $F=false
var $G=false
var $H=false
if($A.number=="1"){return true;}
for(fi=0;fi<4;fi++){if(this.foundations[fi]!==null){if($A.suit==this.foundations[fi].suit&&$A.number=="2"&&this.foundations[fi].number=="1"){return true;}
else if($E=="red"&&
this.foundations[fi].suit=='c'&&
parseInt(this.foundations[fi].number)>=parseInt($A.number)-1){$F=true;}
else if($E=="red"&&
this.foundations[fi].suit=='s'&&
parseInt(this.foundations[fi].number)>=parseInt($A.number)-1){$G=true;}
else if($E=="black"&&
this.foundations[fi].suit=='h'&&
parseInt(this.foundations[fi].number)>=parseInt($A.number)-1){$F=true;}
else if($E=="black"&&
this.foundations[fi].suit=='d'&&
parseInt(this.foundations[fi].number)>=parseInt($A.number)-1){$G=true;}
if(this.foundations[fi].suit==$A.suit&&parseInt(this.foundations[fi].number)+1==parseInt($A.number)){$H=true;}}}
if($F==true&&$G==true&&$H==true){return true;}else{return false;}}
Board.prototype.GetNumOpenCols=function(){var $I=0;for(i=0;i<this.board.length;i++){if(this.board[i].length==0){$I++;}}
return $I;}
Board.prototype.GetNumOpenFreeCells=function(){var $I=0;for(i=0;i<this.freeCells.length;i++){if(this.freeCells[i]==null){$I++;}}
return $I;}
function Card($J,$K){Card.width=72;Card.height=96;this.number=""+$J;this.suit=$K;}
Card.prototype.GetColor=function(){return((this.suit=='c'||this.suit=='s')?"black":"red");}
Card.prototype.Clone=function(){return new Card(this.number,this.suit);}
Card.prototype.GetImageCoords=function($L){var w=72;var h=96;var $M="shcd";var x=(this.number-1)*w;if(!$L){var y=$M.indexOf(this.suit)*h*2;}else{var y=($M.indexOf(this.suit)*h*2)+h;}
return[x,y,w,h];}
function ClearChildren(el){if(el.hasChildNodes()){while(el.childNodes.length>=1){el.removeChild(el.firstChild);}}}
var $N=null;function init(){var $O=document.getElementById('toolbar').getElementsByTagName('a');for(var i=0;i<$O.length;i++){$O[i].onselectstart=function(){return false;};$O[i].unselectable="on";$O[i].style.MozUserSelect="none";$O[i].style.cursor="default";}
$N=new Board('board');$N.Deal();}
