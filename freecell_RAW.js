//Javascript/HTML Freecell Program
//Created by Eric Goldner

if(typeof($)!="function") {
  $ = function(id) { return document.getElementById(id); }
}

Array.prototype.Copy = function()
{
  if(this.length == 0) {
    //Empty array
    return [];
  }
  else if(typeof(this[0]) == "object")
  {
    //2D array (the board)
    var ret = new Array(this.length);
    for(var subindex = 0; subindex < this.length; subindex++)
    {
      //there is a simple null in the array      
      if(this[subindex] === null) {
          ret[subindex] = null;
      }
      else if (this[subindex].constructor.toString().indexOf("Array") != -1)
      {      
        //there is an array inside the array
        ret[subindex] = [];
        for(var y = 0; y < this[subindex].length; y++) {
          if(typeof(this[subindex][y].Clone) == "function") {
            ret[subindex].push(this[subindex][y].Clone());
          }
          else if (this[subindex][y] === null) {
            ret[subindex].push(null);
          }
        }   
      } else if(typeof(this[subindex].Clone) == "function") {
        ret[subindex] = this[subindex].Clone();
      }
    }
    return ret;
  }
}

var zy = document.domain;

Array.prototype.shuffle = function(arr)
{
  var i = this.length, j, t;
  while( i ) {
    j = Math.floor( ( i-- ) * Math.random() );
    t = arr && typeof this[i].shuffle!=='undefined' ? this[i].shuffle() : this[i];
    this[i] = this[j];
    this[j] = t;
  }
  return this;
}

function Board(boardDiv)
{
  this.boardId = boardDiv 
  //sets the number of columns in the board 
  this.board = new Array(8);
  for(i=0;i<this.board.length;i++) {
      this.board[i] = [];    
  }
  
  this.initialBoard = null;
  this.previousBoard = null;
  
  //number of freecells (upper-left)
  this.freeCells = null;
  
  //set
  this.foundations = null;
  this.selectedCard = null; 
  
  this.lastClickTime = null;
}

Board.prototype.Deal = function()
{
  //clear any other variables
  //number of freecells (upper-left)
  this.freeCells = [null,null,null,null];
  
  //set
  this.foundations = [null,null,null,null];
  this.selectedCard = [null, null]; 
  
  this.lastClickTime = null;
  
  
  //set internal things
  var queue = [];
  var nums = [1,2,3,4,5,6,7,8,9,10,11,12,13];
  var suits = ['s','c','h','d'];
  
  var card = null;
  for(i=0;i<suits.length;i++) {
    for(j=0;j<nums.length;j++) {
      card = new Card(nums[j],suits[i]);
      queue.push(card);
    }
  }

  queue.shuffle();
  queue.shuffle();
  queue.shuffle();
  queue.shuffle();
  
  var j = -1;
  
  //fill the main deck
  while(queue.length > 0)
  {
    j++;
    for(i=0;i<this.board.length;i++) {
      if(queue.length > 0) {
        this.board[i][j] = queue.pop();
      } 
    }
  }
  
  this.initialBoard = [this.freeCells.Copy(), this.foundations.Copy(), this.board.Copy()];
  this.SaveState();
  
  //SAVE this board in case the user wants to restart
  this.Draw();
}

Board.prototype.ShowInitial = function()
{
  var output = [];
  for(var x = 0; x < this.previousBoard[2].length; x++) {
    output.push('<div style="float:left;width:50px;">');
    for(var y = 0; y < this.previousBoard[2][x].length; y++) {
      output.push(this.previousBoard[2][x][y].number, this.previousBoard[2][x][y].suit, "<br>");
    }
    output.push('</div>');
  }
  
  document.getElementById('debug').innerHTML = output.join("");
}

//randomly add this for checking
var aa = "www.";
var cc = ".net";
var dd = "localhost";

Board.prototype.Draw = function()
{
  var img = null;
  var card = null;
  var gameboard = this;
  var i = 0;
  
  var toolbarHeight = 35;
  //Prerendering processing
    
  //FIND ANY POSSIBLE AUTO MOVES TO FOUNDATIONS
  
  //now check the board as often as necessary
  var flag = false;
  do {
    flag = false;
    
    //check all freecells
    for(i = 0; i < this.freeCells.length; i++)
    {
      if(this.freeCells[i] !== null && this.CanAutoMoveToFoundation(this.freeCells[i])) {
        flag = true
        this.MM("fc", i, "found", null, true, false);
      }
    }
    
    for(i = 0; i < this.board.length; i++)
    {
      if(this.CanAutoMoveToFoundation(i)) {
        flag = true
        this.MM("b", i, "found", null, true, false);
      }
    }
  } while(i < 20 && flag == true)

  ClearChildren(document.getElementById(this.boardId));
  
  for(x=0;x<this.freeCells.length;x++)
  {
    img = document.createElement("div");
    img.id = "freecell_" + x;
    img.style.top = toolbarHeight + "px";
    img.style.left = (x * (Card.width + 2)) + "px";
    if(this.freeCells[x] === null) {
      img.className = "placeholder";
    }
    else if(this.selectedCard[0] == 'fc' && this.selectedCard[1] == x) {
      //this.freeCells[x] is a selected card
      img.className = "card";
      var loc = this.freeCells[x].GetImageCoords(true);
      img.style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
    } else {
      //unselected
      img.className = "card";
      var loc = this.freeCells[x].GetImageCoords(false);
      img.style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
    }
    //set custom attributes
    img.col = x;
    img.onmouseover = function(e) {
      document.getElementById('kingshead').className = "kingLookingLeft";
    }
    img.onclick = function(e)
    {
      //are we selected something brand new?
      if(gameboard.selectedCard[0] === null)
      {
        gameboard.selectedCard = ['fc', this.col];
        gameboard.Draw();
        return;
      }
      else 
      {
        //same thing selected now was selected last time as well
        if(!(gameboard.selectedCard[0] == 'fc' && gameboard.selectedCard[1] == this.col)) {
          gameboard.MM(gameboard.selectedCard[0],gameboard.selectedCard[1], "fc", this.col, true, true);
        }
        gameboard.selectedCard = [null, null];
        gameboard.Draw();
      }
    }
    document.getElementById(this.boardId).appendChild(img);
  }
  var startLeft = (this.freeCells.length * (Card.width + 2));
  img = document.createElement("div");
  img.id = "kingshead";
  img.style.position = "absolute";
  img.style.top = toolbarHeight + "px";
  img.style.left = startLeft + "px";
  img.className = "kingLookingLeft";
  document.getElementById(this.boardId).appendChild(img);
  
  //draw foundations
  startLeft = ((1 + this.freeCells.length) * (Card.width + 2));
  for(x=0;x<this.foundations.length;x++)
  {
    img = document.createElement("div");
    img.id = "foundation_" + x;
    img.style.top = toolbarHeight + "px";
    img.style.left = (startLeft + (x * (Card.width + 2))) + "px";
    
    if(this.foundations[x]===null) {
      img.className = "placeholder";
    } else {
      img.className = "card";
      var loc = this.foundations[x].GetImageCoords(false);
      img.style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
    }
    img.col = x;
    
    img.onmouseover = function(e) {
      document.getElementById('kingshead').className = "kingLookingRight";
    }
    
    img.onclick = function(e)
    {
      //are we selected something brand new?
      if(gameboard.selectedCard[0] === null)
      {
        //don't select foundation cards
      }
      else 
      {
        //same thing selected now was selected last time as well
        gameboard.MM(gameboard.selectedCard[0],gameboard.selectedCard[1], "found", this.col, true, true);
        gameboard.selectedCard = [null, null];
        gameboard.Draw();
      }
    }
    
    document.getElementById(this.boardId).appendChild(img);
  }
  
  
  //no cards left in freecells or tableau spaces (GAME WIN)
  if(this.GetNumOpenFreeCells() == this.freeCells.length && this.GetNumOpenCols() == this.board.length)
  {
    document.getElementById('winFace').style.display = "block";
    alert("Congratulations, you win!");
    document.getElementById('winFace').style.display = "none";

    //start a new game
    this.Deal();
    return;
  } else {
    //draw the board, tableau cards
    for(x=0;x<this.board.length;x++)
    { 
      if(this.board[x].length > 0)
      {
        for(y=0;y<this.board[x].length;y++)
        {
          card = this.board[x][y];
          if(card != null)
          {
            img = document.createElement("div");
            img.id = "tableau_" + x + "-" + y;
            
            if(this.selectedCard[0] == 'b' && this.selectedCard[1] == x && y == this.board[x].length - 1) {
              img.className = "card";
              var loc = card.GetImageCoords(true);
              img.style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
            } else {
              img.className = "card";
              var loc = card.GetImageCoords(false);
              img.style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
            }
            img.style.top = toolbarHeight + 100 + (y * 20 + 5) + "px";
            img.style.left =  (x * (7 + Card.width) + 5) + "px";
            img.style.zIndex = y + 10; 
            
            //set custom attributes
            img.col = x;
            img.row = y;
            
            
            if(BD.browser == "Explorer")
            {
              img.ondblclick = function() {
                //it is a double click, try to move the card to its foundation
                if(gameboard.MM("b", this.col, "fc", 0, true, true))
                {
                  /*
                  while(gameboard.CanAutoMoveToFoundation(this.col)) {
                    gameboard.MM("b", this.col, "found", null, true, false);
                  }
                  */
                  
                  gameboard.selectedCard = [null, null];
                  gameboard.Draw();
                }
              }
            }
            
            img.oncontextmenu = function()
            {
              if(this.style.zIndex == 5000) {
                this.style.zIndex = this.row + 10;
              } else {
               this.style.zIndex = 5000;
              } 
              return false;
            }
            
            img.onmouseout = function(e)
            {
              if(this.style.zIndex == 5000) {
                this.style.zIndex = this.row + 10;
              }
            }
  
            img.onclick = function()
            {
              var timeSinceLastClick = gameboard.MCI();
              //are we selected something brand new? (SELECTION)
              if(gameboard.selectedCard[0] === null)
              {
                gameboard.selectedCard = ['b', this.col];
                //var loc = gameboard.board[this.col][this.row].GetImageCoords(true);
                
                var newY = gameboard.board[this.col].length - 1;
                var loc = gameboard.board[this.col][newY].GetImageCoords(true);
                document.getElementById('tableau_' + this.col + '-' + newY).style.backgroundPosition = "-" + loc[0] + "px -" + loc[1] + "px";
                return true;
              }
              else 
              {
                //same thing selected now was selected last time as well
                if(gameboard.selectedCard[0] == 'b' && gameboard.selectedCard[1] == this.col)
                {
                  //is this a dbl click?
                  if(BD.browser != "Explorer" && timeSinceLastClick <= 250) {
                    //it is a double click, try to move the card to its foundation
                    if(gameboard.MM(gameboard.selectedCard[0],gameboard.selectedCard[1], "fc", 0, true, true))
                    {
                      /*
                      while(gameboard.CanAutoMoveToFoundation(this.col)) {
                        gameboard.MM("b", this.col, "found", null, true, false);
                      }
                      */
                    }
                  }
                }
                else
                {
                  //something else was selected last time, try to move
                  gameboard.MM(gameboard.selectedCard[0],gameboard.selectedCard[1], "b", this.col, true, true);
                }
                gameboard.selectedCard = [null, null];
                gameboard.Draw();
              }
            }
            document.getElementById(this.boardId).appendChild(img);
          }
        }
      } else {
        img = document.createElement("div");
        img.className = "placeholder";
        img.style.top = toolbarHeight + 100 + (5) + "px";
        img.style.left =  (x * (7 + Card.width) + 5) + "px";
        
        //set custom attributes
        img.col = x;
        img.row = -1;
        
        img.onclick = function(e)
        {
          var timeSinceLastClick = gameboard.MCI();
          if(gameboard.selectedCard[0] !== null) 
          {
            if(!(gameboard.selectedCard[0] == 'b' && gameboard.selectedCard[1] == this.col))
            {
              //something else was selected last time, try to move
              gameboard.MM(gameboard.selectedCard[0],gameboard.selectedCard[1], "b", this.col, true, true);
            }
            gameboard.selectedCard = [null, null];
            gameboard.Draw();
          }
        }
        document.getElementById(this.boardId).appendChild(img);
      
      }
    }
  }
}

var cc = ".net";

//MeasureClickInterval
Board.prototype.MCI = function()
{
  if(this.lastClickTime === null)
  {
    this.lastClickTime = new Date().getTime();
    return null;
  } else {
    var now = new Date().getTime();
    var ret = now - this.lastClickTime;
    this.lastClickTime = now;
    return ret;
  }
}

//MakeMove
//fromArea can be 'b', 'fc'
//toArea can be 'b', 'fc', 'found'
Board.prototype.MM = function(fromArea, fromCol, toArea, toCol, doIt, doSaveState)
{ 
  //cant transfer to same location
  if(fromArea == toArea && fromCol == toCol)  {
    return false;
  }

  var sourceCards = [];
  var topCard = null;
  var bottomCard = null;
  
  if(fromArea == 'b')
  {
    for(i = this.board[fromCol].length - 1; i >= 0; i--)
    {
      topCard = this.board[fromCol][i];
      if(sourceCards.length > 0) {
        bottomCard = sourceCards[sourceCards.length-1];
      }
      if(bottomCard === null || (parseInt(topCard.number) == parseInt(bottomCard.number) + 1 && topCard.GetColor() != bottomCard.GetColor())) {
        sourceCards.push(topCard);
      } else {
        //only one failure up the chain before we get booted out 
        break;
      }
    }
  }
  else if(fromArea == 'fc') {
    sourceCards.push(this.freeCells[fromCol]);
  }
  
  //no cards to transfer
  if(sourceCards.length == 0 || sourceCards[0] == null) {
    return false;
  }
  
  if(toArea == 'fc')
  {
    var flag = false;

    //first thing to do is check if we can bypass this and send it to a foundation
    if(fromArea == 'b') {
      while(this.CanAutoMoveToFoundation(fromCol)) {
        return this.MM(fromArea, fromCol, "found", 0, doIt);
      }
    }

    for(i = 0; i < this.freeCells.length; i++)
    {
      if(sourceCards.length > 0 && this.freeCells[i] === null)
      {
        if(doIt)
        {
          if(doSaveState) { this.SaveState(); }
          this.freeCells[i] = sourceCards[0];
          if(fromArea == 'fc') {
            //transfer between one freecell and another
            this.freeCells[fromCol] = null;
          }
          else if(fromArea == 'b') {
            //remove the bottom card from that column 
            this.board[fromCol].pop();
          }
        }  
        flag = true;
        break;
      }
    }
    return flag;
  }
  else if(toArea == 'b')
  {
    //the bottom card on the receiving column to receive new card(s)
    var topCard = null;
    
    //if there are any cards on the row, account for them
    if(this.board[toCol].length > 0) {
      topCard = this.board[toCol][this.board[toCol].length - 1];
    }
    
    var maxTransferableCards = 0;
    if(topCard === null) {
      //automatically temporarily use empty columns
      maxTransferableCards = Math.min(sourceCards.length, (this.GetNumOpenFreeCells()+1) * this.GetNumOpenCols());
    } else {
      maxTransferableCards = Math.min(sourceCards.length, (this.GetNumOpenFreeCells()+1) * (this.GetNumOpenCols() + 1));
    }
    
    for(i = maxTransferableCards - 1; i >= 0; i--)
    {
      //the first card that will go onto the new col
      bottomCard = sourceCards[i];

      //are the columns compatible? is the receiving col empty?       
      if(topCard === null || (parseInt(bottomCard.number) + 1 == parseInt(topCard.number) && bottomCard.GetColor() != topCard.GetColor()))
      {
        //there is a match between columns
        if(doIt) {
          if(doSaveState) { this.SaveState(); }
          
          //add the correct number of cards to the new location
          //correct the order of the new cards to the order they will appear on the board
          sourceCards.reverse();
          this.board[toCol] = this.board[toCol].concat(sourceCards.splice(sourceCards.length - (i + 1),i + 1));
          
          //remove the card(s) from wherever they came from
          if(fromArea == 'b') {
            this.board[fromCol].splice(this.board[fromCol].length - (i + 1), i + 1);
          }
          else if(fromArea == 'fc') {
            this.freeCells[fromCol] = null;
          }
        }
        return true;
      }
    }
    
    //if it hasnt returned true by now, assume failure
    return false;
  }
  else if(toArea == 'found')
  {
    bottomCard = sourceCards[0];
    
    for(var fci = 0;fci < 4; fci++)
    {
      //if its an ace, find the 1st empty slot and stick it there
      //else, it must be one higher than the current and the same suit
      if(
        (this.foundations[fci] == null && bottomCard.number == "1") ||
        (this.foundations[fci] != null && parseInt(this.foundations[fci].number) + 1 == parseInt(bottomCard.number) && this.foundations[fci].suit == bottomCard.suit)
      )
      {
        if(doIt)
        {
          if(doSaveState) { this.SaveState(); }
          this.foundations[fci] = bottomCard;
          if(fromArea == 'fc') {
            this.freeCells[fromCol] = null;
          }
          else if(fromArea == 'b') {
            this.board[fromCol].pop();
          }
        }
        return true
      }
    }
    return false;
  }
}

Board.prototype.SaveState = function()
{
  this.previousBoard = [this.freeCells.Copy(), this.foundations.Copy(), this.board.Copy()];
}

Board.prototype.Undo = function()
{
  //revert to older card locations
  if(this.previousBoard == null) {
    return false;
  }
  this.freeCells = this.previousBoard[0].Copy();
  this.foundations = this.previousBoard[1].Copy();
  this.board = this.previousBoard[2].Copy();
  this.previousBoard = null;
  this.Draw();
}

Board.prototype.Restart = function()
{
  if(this.initialBoard == null) {
    return false;
  }
  this.freeCells = this.initialBoard[0].Copy();
  this.foundations = this.initialBoard[1].Copy();
  this.board = this.initialBoard[2].Copy();
  this.Draw();
}

Board.prototype.CanAutoMoveToFoundation = function(col)
{
  var bottomCard = null;
  if(typeof(col)=="number") {
    //col was given as a board column index
    if(this.board[col].length == 0) { return false; }
    //manually set the bottom card from the lowest card in the column index 
    bottomCard = this.board[col][this.board[col].length - 1];
  } else {
    //col was given as a card
    bottomCard = col;
  }

  var color = bottomCard.GetColor();
  var flag_1 = false
  var flag_2 = false
  var flag_3 = false

  //always move an ace  
  if(bottomCard.number == "1") { 
    return true;
  }
  
  for(fi = 0; fi < 4; fi++)
  {
    if(this.foundations[fi] !== null)
    {
      if(bottomCard.suit == this.foundations[fi].suit && bottomCard.number == "2" && this.foundations[fi].number == "1")
      {
        //automatically put a 2 up there if theres already an ace
        return true;
      }
      else if(
        color=="red" && 
        this.foundations[fi].suit == 'c' &&
        parseInt(this.foundations[fi].number) >= parseInt(bottomCard.number) - 1)
      {
        flag_1 = true;
      }
      else if(
        color=="red" && 
        this.foundations[fi].suit == 's' &&
        parseInt(this.foundations[fi].number) >= parseInt(bottomCard.number) - 1)
      {
        flag_2 = true;
      }
      else if(
        color=="black" && 
        this.foundations[fi].suit == 'h' &&
        parseInt(this.foundations[fi].number) >= parseInt(bottomCard.number) - 1)
      {
        flag_1 = true;
      }
      else if(
        color=="black" && 
        this.foundations[fi].suit == 'd' &&
        parseInt(this.foundations[fi].number) >= parseInt(bottomCard.number) - 1)
      {
        flag_2 = true;
      }
      
      if(this.foundations[fi].suit == bottomCard.suit && parseInt(this.foundations[fi].number) + 1 == parseInt(bottomCard.number))
      {
        flag_3 = true;
      }
    }
  }
  
  //this card will never be used by the board, redirect this function to put it in a foundation
  //account for aces here too
  if(flag_1 == true && flag_2 == true && flag_3 == true) {
    return true;
  } else {
    return false;
  }
}

Board.prototype.GetNumOpenCols = function()
{
  var num = 0;
  for(i=0;i<this.board.length;i++) {
    if(this.board[i].length == 0) {
      num++;
    }
  }
  return num;
}

Board.prototype.GetNumOpenFreeCells = function()
{
  var num = 0;
  for(i=0;i<this.freeCells.length;i++) {
    if(this.freeCells[i] == null) {
      num++;
    }
  }
  return num;
}

function Card(number, suit)
{
  Card.width = 72;
  Card.height = 96;
  this.number = "" + number;
  this.suit = suit;  
}

Card.prototype.GetColor = function()
{
  return ((this.suit == 'c' || this.suit == 's')?"black":"red");
}


Card.prototype.Clone = function()
{
  return new Card(this.number, this.suit);
}

Card.prototype.GetImageCoords = function(isSelected)
{
  var w = 72;
  var h = 96;
  var imageOrder = "shcd";
  var x = (this.number - 1) * w;
  if(!isSelected) {
    var y = imageOrder.indexOf(this.suit) * h * 2;
  } else {
    var y = (imageOrder.indexOf(this.suit) * h * 2) + h;
  }
  return [x,y,w,h];
}

function ClearChildren(el)
{
  if (el.hasChildNodes()) {
    while(el.childNodes.length >= 1) {
      el.removeChild(el.firstChild);       
    }
  }
}

var fcboard = null;
function init()
{
  //prevent annoying text selecction on double click
  var els = document.getElementById('toolbar').getElementsByTagName('a');
  for(var i = 0; i < els.length; i++)
  {
    els[i].onselectstart = function() {
      return false;
    };
    els[i].unselectable = "on";
    els[i].style.MozUserSelect = "none";
    els[i].style.cursor = "default";
  }
  
  //someone other than me is using this
  if(zy != bb + cc && zy != aa+bb+cc && zy != dd){return false;}
  
  fcboard = new Board('board');
  fcboard.Deal();
}